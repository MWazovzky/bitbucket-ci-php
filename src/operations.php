<?php

namespace App;

class Operations 
{
    public function add(int $a, int $b) : int
    {
        return $a + $b;
    }
}
