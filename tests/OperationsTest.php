<?php

namespace Test;

use App\Operations;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /** @test */
    public function it_can_add_values() : void
    {
        $this->assertEquals(7, (new Operations)->add(3, 4));
    }
}

